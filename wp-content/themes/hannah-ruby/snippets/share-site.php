<ul class="sharers">
	<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php bloginfo('url'); ?>" target="_blank" class="sharer facebook">Share <i class="fa fa-facebook"></i></a></li>
	<li><a href="https://twitter.com/intent/tweet?text=<?php bloginfo('name'); ?>&url=<?php bloginfo('url'); ?>" class="sharer twitter" target="_blank">Tweet <i class="fa fa-twitter"></i></a></li>
	<li><a href="https://plus.google.com/share?url=<?php bloginfo('url'); ?>" target="_blank" class="sharer google">Plus <i class="fa fa-google-plus"></i></a></li>
</ul>
